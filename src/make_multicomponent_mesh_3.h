// Copyright (C) 2014 Benjamin Kehlet
//
// This file is part of mshr.
//
// mshr is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// mshr is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with mshr.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MSHR_MAKE_MULTICOMPONENT_MESH_3_H
#define __MSHR_MAKE_MULTICOMPONENT_MESH_3_H

#include <CGAL/make_mesh_3.h>
#include <CGAL/refine_mesh_3.h>


template<class C3T3, class MeshDomain, class MeshCriteria>
void make_multicomponent_mesh_3_impl(C3T3& c3t3,
                                     const MeshDomain&   domain,
                                     const MeshCriteria& criteria,
                                     const bool with_features)
{
  //std::cout << "Number of vertices initially: " << c3t3.triangulation().number_of_vertices() << std::endl;

  // Initialize c3t3 with points from the special features
  CGAL::Mesh_3::internal::C3t3_initializer< 
    C3T3,
    MeshDomain,
    MeshCriteria,
#if CGAL_VERSION_NR >= 1050601000
    CGAL::internal::has_Has_features<MeshDomain>::value > () (c3t3,
                                                              domain,
                                                              criteria,
                                                              with_features);
#else
    CGAL::Mesh_3::internal::has_Has_features<MeshDomain>::value > () (c3t3,
                                                              domain,
                                                              criteria,
                                                              with_features);
#endif

  // std::cout << "Number of vertices after features: " << c3t3.triangulation().number_of_vertices() << std::endl;
  
  // Inserts points from all connected components to the mesh
  CGAL::Mesh_3::internal::init_c3t3(c3t3, domain, criteria, 0);
  // std::cout << "Number of vertices before meshing: " << c3t3.triangulation().number_of_vertices() << std::endl;
  
  // Build mesher and launch refinement process
  // Don't reset c3t3 as we just created it
  refine_mesh_3(c3t3, domain, criteria,
		CGAL::parameters::no_exude(),
		CGAL::parameters::no_perturb(),
		CGAL::parameters::no_odt(),
		CGAL::parameters::no_lloyd());
}

#endif
